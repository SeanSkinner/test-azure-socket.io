# AssignmentOneStockPrice

AssignmentOneStockPrice is a Web application written in Javascript using NodeJS. This project is the first assignment in our crash course and my first Git project. The application will present users with the current stock price for 3 different stocks. These prices should be possible to update using PATCH requests, and fetch using GET requests.

## Installation

Use the package manager npm to install the applications required packages

```bash
npm install
```

## Usage

Start the application using npm start
```bash
npm start
``` 
Enter webpage [localhost:3000](http://localhost/3000) 
> **_NOTE:_**  Port 3000 is the default port. If your process environment variables have another port specified, use that port instead.

To update stockprice data, send a PATCH request to: http://localhost/3000/update 
The PATCH request must provide a JSON body in the following format: 

```JSON
{
    "data": {
        "stockName": "ACME Traders",
        "price": 44.40
    }
}
```

To GET the latest stock/price data, send a GET request to: http://localhost/3000/current
You will recieve all current stocks and prices in a JSON format.

## Feedback
Please provide any feedback for improvements!
